package com.example.m2;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.RatingBar;
import android.widget.VideoView;
;

public class Video_Activity extends AppCompatActivity {

    VideoView videoview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_);
        ActionBar actionBar= getSupportActionBar();
        actionBar.setTitle("Video");
        String ruta = "android.resource://" + getPackageName() + "/" + R.raw.gato2;
        videoview = (VideoView) findViewById(R.id.videoView);
        Uri uri = Uri.parse(ruta);
        videoview.setVideoURI(uri);

        MediaController mediaController = new MediaController(this);
        videoview.setMediaController(mediaController);
        mediaController.setAnchorView(videoview);

    }
}
