package com.example.m2;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.example.m2.Image_grid.Image_Activity;

public class Menu_Activity extends AppCompatActivity {
    Button calculatorButton;
    Button webButton;
    Button imageButton;
    Button videoButton;
    Button locoButton;
    Button salirButton;
    Button aboutButton;
    Button gpsButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_);
        ActionBar actionBar= getSupportActionBar();
        actionBar.setTitle("Menu");
        calculatorButton=(findViewById(R.id.BtnCalculator));
        webButton=(findViewById(R.id.BtnWeb));
        imageButton=(findViewById(R.id.BtnImages));
        videoButton=(findViewById(R.id.BtnVideo));
        locoButton=(findViewById(R.id.BtnLoco));
        salirButton=(findViewById(R.id.BtnSalirMenu));
        aboutButton=(findViewById(R.id.BtnAcerca));
        gpsButton=findViewById(R.id.BtnGps);
        salirButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishAffinity();
            }
        });
        locoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (view.getContext(), Loco_Activity.class);
                startActivity(intent);
            }
        });

        calculatorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (view.getContext(), Caculator_Activity.class);
                startActivity(intent);
            }
        });
        webButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (view.getContext(), Web_Activity.class);
                startActivity(intent);
            }
        });
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (view.getContext(), Image_Activity.class);
                startActivity(intent);
            }
        });
        videoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (view.getContext(), Video_Activity.class);
                startActivity(intent);
            }
        });
        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialog();
            }
        });
        gpsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (view.getContext(), Gps_view.class);
                startActivity(intent);
            }
        });
    }
    public void showAlertDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Acerca de ")
                .setMessage("Nombre del alumnno: Diego Omalli \n" +
                        "21 años del Olimpo, Estado de México\n"+
                        "Para la materia de Programacion Aplicada a Sistemas de Computo Movil \n" +
                        "Profesora: Rocio Elizabeth Pulido" +
                        "Periodo 2021B"
                )
                .setPositiveButton("Aceptar", null).show();
    }
}