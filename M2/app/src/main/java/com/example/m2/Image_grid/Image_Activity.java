package com.example.m2.Image_grid;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;


import com.example.m2.R;

public class Image_Activity extends AppCompatActivity  {
    private GridView gridView;
    private AdaptadorDeCoche adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_);
        ActionBar actionBar= getSupportActionBar();
        actionBar.setTitle("Imagen");

        gridView = (GridView) findViewById(R.id.grid);
        gridView.setAdapter(new AdaptadorDeCoche(this));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(),Full_screen_Activity.class);
                intent.putExtra("idimagen",i);
                startActivity(intent);
            }
        });

    }

}