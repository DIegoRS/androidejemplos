package com.example.m2.Image_grid;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.m2.R;

public class Full_screen_Activity extends AppCompatActivity {
    ImageView imageDetails;
    AdaptadorDeCoche adaptadorDeCoche;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_);
        imageDetails=(findViewById(R.id.imagen_detalle));
        ActionBar actionBar= getSupportActionBar();
        actionBar.setTitle("Full screen image");
        Intent intent= getIntent();
        int position=intent.getExtras().getInt("idimagen");
        adaptadorDeCoche= new AdaptadorDeCoche(this);
        imageDetails.setImageResource(adaptadorDeCoche.getItem(position).getIdDrawable());


    }
}