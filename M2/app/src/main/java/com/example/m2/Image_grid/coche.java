package com.example.m2.Image_grid;

import com.example.m2.R;

public class coche {
    private String nombre;
    private int idDrawable;

    public coche(String nombre, int idDrawable) {
        this.nombre = nombre;
        this.idDrawable = idDrawable;
    }

    public String getNombre() {
        return nombre;
    }

    public int getIdDrawable() {
        return idDrawable;
    }

    public int getId() {
        return nombre.hashCode();
    }

    public static coche[] ITEMS = {
            new coche("Jaguar F-Type 2015", R.drawable.jaguaf),
            new coche("Mercedes AMG-GT", R.drawable.merdez),
            new coche("Mazda MX-5", R.drawable.amzada),
            new coche("Porsche 911 GTS", R.drawable.poshe),
            new coche("BMW Serie 6", R.drawable.bmw),
            new coche("Ford Mondeo", R.drawable.fordmondeohybrid3),
            new coche("Volvo V60 Cross Country", R.drawable.volvo),
            new coche("Jaguar XE", R.drawable.jaguar),
            new coche("VW Golf R Variant", R.drawable.golf),
            new coche("Seat León ST Cupra", R.drawable.leon),
    };

    /**
     * Obtiene item basado en su identificador
     *
     * @param id identificador
     * @return Coche
     */
    public static coche getItem(int id) {
        for (coche item : ITEMS) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
}