package com.example.m2;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

public class Loco_Activity extends AppCompatActivity {
    TextView editHora;
    Calendar calendar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loco_);
        ActionBar actionBar= getSupportActionBar();
        actionBar.setTitle("Loco");
        ImageView simple_image = findViewById(R.id.imageView);///id imageview
        SeekBar sbar = findViewById(R.id.seekBar);
        TextView contr = findViewById(R.id.textView2);
        RatingBar rb= findViewById(R.id.ratingBar2);
        editHora=findViewById(R.id.textViewHora);
        calendar= Calendar.getInstance();
        Date currentTime=calendar.getTime();
        editHora.setText(currentTime.getHours()+":"+currentTime.getMinutes());
        rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                String mess="";
                switch ((int) rb.getRating()){
                    case 0:
                        mess="Really Bad";
                        break;
                    case 1:
                        mess="Bad";
                        break;
                    case 2:
                        mess="Regular";
                        break;
                    case 3:
                        mess="Fine";
                        break;
                    case 4:
                        mess="Good";
                        break;
                    case 5:
                        mess="Very good";
                        break;
                }
                tostMessage(mess);
            }
        });
        editHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int hora=calendar.get(Calendar.HOUR_OF_DAY);
                int minutes=calendar.get(Calendar.MINUTE);
                setTime(hora,minutes);

            }
        });
        sbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @SuppressLint("SetTextI18n")
            @Override
            public  void onProgressChanged(SeekBar seekBar, int progress, boolean b){
                simple_image.setImageBitmap(changeBitmapContrastBrightness(BitmapFactory.decodeResource(getResources(), R.drawable.ai_iage2), (float) progress / 100f, 1));
                contr.setText("Contraste: "+(float) progress / 100f);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        sbar.setMax(200);
        sbar.setProgress(100);
    }
    public static Bitmap changeBitmapContrastBrightness(Bitmap bmp, float contrast, float brightness) {
        ColorMatrix cm = new ColorMatrix(new float[]
                {
                        contrast, 0, 0, 0, brightness,
                        0, contrast, 0, 0, brightness,
                        0, 0, contrast, 0, brightness,
                        0, 0, 0, 1, 0
                });
        Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());
        Canvas canvas = new Canvas(ret);
        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        canvas.drawBitmap(bmp, 0, 0, paint);
        return ret;
    }
   private void  tostMessage(String message){
       Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
       toast.show();
    }
    private  void setTime(int hora,int minutes){
        TimePickerDialog timerpicker= new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                editHora.setText(i+":"+i1);
            }
        },hora,minutes,false);
        timerpicker.show();
    }

}