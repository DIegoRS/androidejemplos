package com.example.m2.Image_grid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.m2.R;

public class AdaptadorDeCoche extends BaseAdapter {
    private Context context;
    public AdaptadorDeCoche(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return coche.ITEMS.length;
    }

    @Override
    public coche getItem(int position) {
        return coche.ITEMS[position];
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_item, viewGroup, false);
        }
        ImageView imagenCoche = (ImageView) view.findViewById(R.id.imagen_coche);
        TextView nombreCoche = (TextView) view.findViewById(R.id.nombre_coche);
        final coche itemCoche=getItem(position);

       // ImageView imagenCoche = new ImageView(context);
       // final coche itemCoche=getItem(position);
       // imagenCoche.setImageResource(itemCoche.getIdDrawable());
       // imagenCoche.setScaleType(ImageView.ScaleType.CENTER_CROP);
       // imagenCoche.setLayoutParams(
       //         new GridView.LayoutParams(150,150));
      //  final coche item = getItem(position);

        Glide.with(imagenCoche.getContext())
               .load(itemCoche.getIdDrawable())
                .into(imagenCoche);
        nombreCoche.setText(itemCoche.getNombre());

        return view;

    }
}
