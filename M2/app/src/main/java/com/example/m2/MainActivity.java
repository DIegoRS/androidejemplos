package com.example.m2;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    Button exitApp;
    Button startSession;
    EditText inputUser;
    EditText inputPassword;
    final String ERROR_MESSAGE="Error de Inicio de Sesion";
    final String SUCCESS_MESSAGE="Inicio de Session correcto";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar= getSupportActionBar();
        actionBar.setTitle("Login");
        startSession=(findViewById(R.id.BtnInciar));
        exitApp=(findViewById(R.id.BtnSalir));
        inputUser=(EditText)(findViewById(R.id.EdtUser));
        inputPassword=(EditText)(findViewById(R.id.EdtPassword));
        exitApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishAffinity();

            }
        });
        startSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    String textUser=inputUser.getText().toString();
                    String textPassword=inputPassword.getText().toString();
                    if(!textUser.trim().equals("") && !textPassword.trim().equals("")){
                        if(textUser.equals("Diego") && textPassword.equals("hola")){
                            Intent intent = new Intent (view.getContext(), Menu_Activity.class);
                            startActivity(intent);
                        }else{
                            showDialog(ERROR_MESSAGE,"Error");
                        }
                    }else{
                            showDialog("El campo no pude ser vacio","Error");
                    }

            }
        });
    }
    public void showDialog(String dialogMessage, String dialogTitle){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(dialogTitle);
        builder.setMessage(dialogMessage);
        builder.setPositiveButton("Aceptar", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}